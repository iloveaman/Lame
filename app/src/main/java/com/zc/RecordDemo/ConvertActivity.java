package com.zc.RecordDemo;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kocla.record.R;
import com.zc.RecordDemo.view.VisualizerView;
import com.zc.RecordDemo.view.WaveformView;

/**
 * 7/3/14  2:42 PM
 * Created by JustinZhang.
 */
public class ConvertActivity extends Activity implements View.OnClickListener, Mp3AudioRecorder.OnRecordListener {

    private static final String TAG = "ConvertActivity";
    private Mp3AudioRecorder recorder;

    private WaveformView waveformView;
    private VisualizerView mBaseVisualizerView;
    private LinearLayout visualizerViewLayout;
    private MediaPlayer mMediaPlayer;//音频
    private Visualizer mVisualizer;//频谱器
    private Equalizer mEqualizer; //均衡器
    private MediaPlayerManager mediaPlayerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "onCreate");
        setContentView(R.layout.convert_layout);
        waveformView = (WaveformView) findViewById(R.id.waveView);
        visualizerViewLayout = (LinearLayout) findViewById(R.id.visualizerViewLayout);
        waveformView.clearAmplitude();
        //recorder = new Mp3AudioRecorder();
        recorder = new Mp3AudioRecorder(this);

        mediaPlayerManager = new MediaPlayerManager(this);
        mMediaPlayer = mediaPlayerManager.getMediaPlayer();
        setupVisualizerFxAndUi();

        findViewById(R.id.stop_record).setOnClickListener(this);
        findViewById(R.id.pause).setOnClickListener(this);
        findViewById(R.id.record).setOnClickListener(this);
        findViewById(R.id.restart).setOnClickListener(this);
        findViewById(R.id.play_record).setOnClickListener(this);
    }

    private static final float VISUALIZER_HEIGHT_DIP = 150f;//频谱View高度

    /**
     * 生成一个VisualizerView对象，使音频频谱的波段能够反映到 VisualizerView上
     */
    private void setupVisualizerFxAndUi() {
        mBaseVisualizerView = new VisualizerView(this);
        mBaseVisualizerView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.FILL_PARENT,//宽度
                (int) (VISUALIZER_HEIGHT_DIP * getResources().getDisplayMetrics().density)//高度
        ));
        //将频谱View添加到布局
        visualizerViewLayout.addView(mBaseVisualizerView);
        //实例化Visualizer，参数SessionId可以通过MediaPlayer的对象获得
        mVisualizer = new Visualizer(mMediaPlayer.getAudioSessionId());
        //采样 - 参数内必须是2的位数 - 如64,128,256,512,1024
        mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
        //设置允许波形表示，并且捕获它
        mBaseVisualizerView.setVisualizer(mVisualizer);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
        recorder.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "ON RESUMME");
        recorder.prepare();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        recorder.release();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pause:
                recorder.pauseRecording();
                break;
            case R.id.record:
                recorder.startRecording();
                break;
            case R.id.stop_record:
                recorder.stopRecording();
                break;
            case R.id.restart:
                recorder.restartRecording();
                break;
            case R.id.play_record:
                mediaPlayerManager.start();
                break;
        }
    }

    @Override
    public void onRecordBegin() {
        Log.i(TAG, "录音开始");
    }

    @Override
    public void onMp3VolumeCallback(int volume) {
        Log.i(TAG, "音量" + volume);
        waveformView.updateAmplitude(volume * 0.1f / recorder.getMaxVolume());
    }

    @Override
    public void onRecordPause() {
        waveformView.clearAmplitude();
        Log.i(TAG, "录音暂停");
    }

    @Override
    public void onRecordResume() {
        Log.i(TAG, "重新录音");
    }

    @Override
    public void onConvertBegin() {
        waveformView.clearAmplitude();
        Log.i(TAG, "录音结束，转换录音开始");
    }

    @Override
    public void onConvertFinish(String path) {
        waveformView.clearAmplitude();
        mediaPlayerManager.setDataSource(path);
        Log.i(TAG, "转换录音完成,目标文件存放在" + path);
    }
}
